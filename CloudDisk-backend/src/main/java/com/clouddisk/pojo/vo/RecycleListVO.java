package com.clouddisk.pojo.vo;


import lombok.Data;

/**
 * @author zhuZhaoYang
 * @date 2022/5/23 14:39
 */
@Data
public class RecycleListVO {
    private Long recycleId;
    private String name;
    private Integer type;
}
