package com.clouddisk.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class FileDTO {
    @NotBlank(message = "文件名不能为空")
    private String name;
    @NotBlank(message = "文件key值不能为空")
    private String key;
    private Long dirId;
}
