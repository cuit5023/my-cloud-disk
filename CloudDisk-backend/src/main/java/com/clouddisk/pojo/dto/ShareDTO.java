package com.clouddisk.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class ShareDTO {
    @NotNull(message = "文件Id不能为空")
    private Long fileId;
    @NotNull(message = "分享天数不能为空")
    private Integer shareDays;

    private String password;
}
