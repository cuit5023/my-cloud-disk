package com.clouddisk.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class DirDTO {
    @NotBlank(message = "目录名不能为空")
    private String name;
    private Long parentId;
}
