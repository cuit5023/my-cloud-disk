package com.clouddisk.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;


@Data
public class SaveShareDTO {
    @NotBlank(message = "token不能为空")
    private String token;
    private Long dirId;
}
