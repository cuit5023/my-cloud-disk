package com.clouddisk.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;


@Data
public class FileMoveDTO {
    @NotNull(message = "文件Id不能为空")
    private Long fileId;
    private Long dirId;
}
