package com.clouddisk.pojo.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DestroyDTO {
    @NotNull(message = "recycleId不能为空")
    private Long recycleId;
}
