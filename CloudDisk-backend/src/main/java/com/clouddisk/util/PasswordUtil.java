package com.clouddisk.util;

import cn.dev33.satoken.secure.SaSecureUtil;
import com.clouddisk.pojo.dto.UAndP;
import org.springframework.stereotype.Component;

@Component
public class PasswordUtil {

    public static String md5BySalt(UAndP uAndP) {
        // md5加盐加密: md5(md5(str) + md5(salt)) password+username
        return SaSecureUtil.md5BySalt(uAndP.getPassword(), uAndP.getUsername());
    }

}
