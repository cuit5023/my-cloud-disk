package com.clouddisk.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clouddisk.pojo.entity.DirEntity;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DirDao extends BaseMapper<DirEntity> {
}
