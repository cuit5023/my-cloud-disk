package com.clouddisk.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.clouddisk.pojo.entity.RecycleEntity;
import com.clouddisk.pojo.vo.RecycleListVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface RecycleDao extends BaseMapper<RecycleEntity> {
    @Select("select id as recycleId ," +
            "IF(fileId is not null," +
            "(select name from file where file.id=fileId)," +
            "(select name from dir where dir.id = dirId)) as name ," +
            "IF(fileId is null,0,1) as type " +
            "from recycle " +
            "where userId = #{userId} " +
            "and status = 0 " +
            "and clearTime > now() " +
            "order by type,name")
    List<RecycleListVO> getList(Long userId);
}
