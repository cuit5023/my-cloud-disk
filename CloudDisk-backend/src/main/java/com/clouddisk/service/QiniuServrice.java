package com.clouddisk.service;

import com.clouddisk.pojo.vo.QiniuPolicyVO;
import com.qiniu.storage.model.FileInfo;


public interface QiniuServrice {

    /**
     * 删除七牛云文件
     */
    Boolean deleteFile(String key);

    /**
     * 检查文件是否存在于七牛云
     */
    FileInfo checkFile(String key);

    /**
     * 前端上传凭证
     */
    QiniuPolicyVO getQiniuPolicy(String loginId);

    String getDownloadUrl(String key,String name);
}
