package com.clouddisk.service;

import com.clouddisk.pojo.entity.RecycleEntity;
import com.clouddisk.pojo.vo.RecycleListVO;

import java.util.List;


public interface RecycleService {
    List<RecycleListVO> getList(Long userId);

    RecycleEntity getById(Long recycleId);

    void recovery(Long recycleId);

    void destroy(Long recycleId);
}
