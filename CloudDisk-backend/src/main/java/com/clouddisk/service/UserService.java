package com.clouddisk.service;

import com.clouddisk.pojo.dto.RegisterDTO;
import com.clouddisk.pojo.entity.UserEntity;
import com.clouddisk.pojo.vo.UserVO;

import java.util.List;

public interface UserService {

    /**
     * 根据用户名获取用户信息
     * @param username 用户名
     * @return 用户实体
     */
    UserEntity getUserByUsername(String username);

    void saveUser(RegisterDTO registerDTO);

    void updateName(String name);

    void updatePassword(String password);

    List<UserVO> getUserList();

    void updateStatus(Long userId, Integer code);
}
