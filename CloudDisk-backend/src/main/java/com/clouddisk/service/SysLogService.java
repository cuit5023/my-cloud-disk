package com.clouddisk.service;

import com.clouddisk.pojo.entity.SysLogEntity;

import java.util.List;

public interface SysLogService {
    /**
     * 新增日志
     *
     * @param sysLogEntity 日志实体
     */
    void addLog(SysLogEntity sysLogEntity);

    List<SysLogEntity> getLogs();
}
