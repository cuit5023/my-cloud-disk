package com.clouddisk.service;

import com.clouddisk.pojo.entity.ShareEntity;
import com.clouddisk.pojo.vo.ShareListVO;
import com.clouddisk.pojo.vo.ShareVO;

import java.util.List;


public interface ShareService {
    /**
     * 分享文件
     *
     * @param fileId    文件
     * @param userId    分享人
     * @param shareDays 分享时间
     * @param password  密码 可空
     * @return 成功信息
     */
    ShareVO shareFile(Long fileId, Long userId, Integer shareDays, String password);

    ShareEntity getShareByToken(String token);

    List<ShareListVO> getShareList(Long userId);

    ShareEntity getShare(Long shareId);

    void updateShare(ShareEntity shareEntity);
}
