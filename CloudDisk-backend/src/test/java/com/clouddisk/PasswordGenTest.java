package com.clouddisk;

import com.clouddisk.pojo.dto.UAndP;
import com.clouddisk.util.PasswordUtil;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author zhuZhaoYang
 * @date 2022/5/14 20:54
 */
@SpringBootTest
public class PasswordGenTest {

    @Test
    void genMd5() {
        String user = PasswordUtil.md5BySalt(new UAndP("user", "123456"));
        String admin = PasswordUtil.md5BySalt(new UAndP("admin", "123456"));
        System.out.println("user=>" + user);
        System.out.println("admin=>" + admin);
    }
}
