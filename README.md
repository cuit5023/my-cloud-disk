1. # 企业网盘项目——汤臣一品业组（34组）

    ## 技术架构

    ### 前端

    - Vue
    - Vue-Router
    - WebPack
    - NodeJS
    - Element UI

    ### 后端

    - SpringBoot
    - MyBatis-Plus
    - Jwt
    - Redis
    - 七牛云对象存储

    ## 项目结构

    ### 前端

    ```tex
    heart-cloud-app
    ├─ .gitignore
    ├─ .npmrc
    ├─ babel.config.js
    ├─ heart-cloud-app.iml
    ├─ package-lock.json
    ├─ package.json
    ├─ postcss.config.js
    ├─ public
    │  ├─ favicon.ico
    │  └─ index.html
    ├─ README.md
    ├─ src
    │  ├─ api
    │  │  ├─ admin.js
    │  │  ├─ file.js
    │  │  ├─ login.js
    │  │  ├─ policy.js
    │  │  ├─ recycle.js
    │  │  ├─ share.js
    │  │  └─ user.js
    │  ├─ App.vue
    │  ├─ assets
    │  │  ├─ dir.png
    │  │  ├─ empty.png
    │  │  ├─ file.png
    │  │  ├─ login.jpg
    │  │  ├─ logo.png
    │  │  ├─ user.png
    │  │  └─ wjj.png
    │  ├─ components
    │  │  ├─ Aside.vue
    │  │  └─ gotop
    │  │     └─ GoTop.vue
    │  ├─ main.js
    │  ├─ request
    │  │  ├─ index.js
    │  │  └─ token.js
    │  ├─ router
    │  │  └─ index.js
    │  ├─ util
    │  │  └─ uuid.js
    │  └─ views
    │     ├─ admin
    │     │  ├─ css
    │     │  │  ├─ global.css
    │     │  │  └─ global.scss
    │     │  ├─ layout.vue
    │     │  ├─ log.vue
    │     │  ├─ login.vue
    │     │  └─ user.vue
    │     ├─ Home.vue
    │     ├─ Index.vue
    │     ├─ Login.vue
    │     ├─ NotFound.vue
    │     ├─ Recycle.vue
    │     ├─ Register.vue
    │     └─ Share.vue
    ├─ static
    │  └─ img
    │     ├─ 404.png
    │     └─ logo.png
    └─ vue.config.js
    ```

    ### 后端

    ```tex
    heart-cloud-serve
    ├─src
    │  ├─main
    │  │  ├─java
    │  │  │  └─com
    │  │  │      └─heartcloud
    │  │  │          ├─config
    │  │  │          ├─controller
    │  │  │          ├─exception
    │  │  │          ├─mapper
    │  │  │          ├─pojo
    │  │  │          │  ├─dto
    │  │  │          │  ├─entity
    │  │  │          │  └─vo
    │  │  │          ├─scheduler
    │  │  │          ├─service
    │  │  │          │  └─impl
    │  │  │          └─util
    │  │  └─resources
    │  └─test
    │      └─java
    │          └─com
    │              └─heartcloud
    └─target
        ├─classes
        │  └─com
        │      └─heartcloud
        │          ├─config
        │          ├─controller
        │          ├─exception
        │          ├─mapper
        │          ├─pojo
        │          │  ├─dto
        │          │  ├─entity
        │          │  └─vo
        │          ├─scheduler
        │          ├─service
        │          │  └─impl
        │          └─util
        ├─generated-sources
        │  └─annotations
        ├─generated-test-sources
        │  └─test-annotations
        └─test-classes
            └─com
                └─heartcloud
    ```

    ## 本地部署

    ### 前端

    下载heart-cloud-frontend，VsCode启动。

    #### 下载依赖

    ```shell
    npm install
    ```

    **注意：如遇见下载依赖报错，考虑npm版本过高的问题，适当降低npm的版本。**

    **解决方案：**使用nvm进行Node JS的版本控制。具体请参考博客：[如何把已安装的nodejs高版本降级为低版本(图文教程)](https://blog.csdn.net/qq_46372463/article/details/125357226?spm=1001.2014.3001.5506)

    #### 启动项目
    
    ```shell
    npm run serve
    ```

    ![image-20221129012142981](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20221129012142981.png)

    **用户**
    
    - 用户名：cxg
    - 密码：123456

    **管理员**
    
    - 用户名:  qyh110
    - 密码：123456

    ### 后端

    #### 1.打开项目

    ![image-20221129171415928](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20221129171415928.png)

    ![image-20221129171925259](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20221129171925259.png)

    #### 2.maven配置

    ##### 1.下载群文件中的maven包

    ![image-20220416223748712](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20220416223748712.png)

    ##### 2.在IDEA中配置maven

    ![image-20220416223825183](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20220416223825183.png)

    ![image-20220416223842703](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20220416223842703.png)

    这样maven就算是基本上配好了

    #### 3.redis配置和数据库配置 (已部署到服务器) 

    ![image-20221129010043904](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20221129010043904.png)

    ### 4.运行项目
    
    ![image-20221128211304433](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20221128211304433.png)
    
    ### 5.成功运行
    
    ![](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20221129010934075.png)
    
    第一次启动项目涉及到maven依赖下载，很慢，等一下就行了。


## 项目概览

![image-20221129012628193](https://jhblog-image.oss-cn-chengdu.aliyuncs.com/blogImg/image-20221129012628193.png)

